#! /usr/bin/env python

score = 0

with open("input.txt") as f:
    ct = 0
    groups = []
    
    for line in f:
        if (ct == 0):
            groups.append([])
        line = line.strip()
        groups[-1].append(set(line))
        ct += 1
        if (ct == 3):
            ct = 0

    for group in groups:
        intesect = group[0].intersection(group[1])
        common = intesect.intersection(group[2]).pop()

        if common.isupper():
            score += ord(common)-64+26
        else:
            score += ord(common)-96
    
print(score)