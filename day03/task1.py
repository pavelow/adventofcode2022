#! /usr/bin/env python

score = 0

with open("input.txt") as f:
    for line in f:
        line = line.strip()
        l = len(line)//2
        common = (set(line[:l]).intersection(set(line[l:]))).pop()
        
        if common.isupper():
            score += ord(common)-64+26
        else:
            score += ord(common)-96

print(score)