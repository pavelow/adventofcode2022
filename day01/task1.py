#! /usr/bin/env python

sums = []

with open("input.txt") as f:
    s = 0
    for line in f:
        if line == "\n":
            sums.append(s)
            s = 0
        else:
            s += int(line)
    sums.append(s)

print(max(sums))
