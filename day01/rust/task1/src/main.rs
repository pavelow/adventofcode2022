use std::io::BufReader;
use std::io::BufRead;
use std::io;
use std::fs;

fn file_to_vec(filename: String) -> io::Result<Vec<String>> { 
    let file_in = fs::File::open(filename)?; 
    let file_reader = BufReader::new(file_in); 
    Ok(file_reader.lines().filter_map(io::Result::ok).collect()) 
} 

fn main() {
    let lines = file_to_vec("../../input.txt".to_string()).unwrap();
    let mut sums : Vec<i32> = Vec::new();

    let mut s = 0;

    for line in lines {
        if line != "" {
            let v: i32 = line.parse().unwrap();
            s += v;
        } else {
            sums.push(s);
            s = 0;
        }
    }
    
    println!("{:?}", sums.iter().fold(std::i32::MIN, |a,b| a.max(*b)));
}
