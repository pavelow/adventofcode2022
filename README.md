# Advent of Code 2022

Advent of code is a yearly advent calendar with new programming problems each day.
This repository contains my solutions to the [AoC 2022 problems](https://adventofcode.com/2022).

My 2021 solutions can be found [here](https://gitlab.com/pavelow/adventofcode2021).

