#! /usr/bin/env python

pts = {'A':1, 'B':2, 'C':3}

win = {'A':{'A':3, 'B':0, 'C':6},
       'B':{'A':6, 'B':3, 'C':0},
       'C':{'A':0, 'B':6, 'C':3}}

winmap = {'A':{'X':'C', 'Y':'A', 'Z':'B'},
          'B':{'X':'A', 'Y':'B', 'Z':'C'},
          'C':{'X':'B', 'Y':'C', 'Z':'A'}}

score = 0
ct = 0
with open("input.txt") as f:
    for line in f:
        r = line.strip().split(" ")
        
        r[1] = winmap[r[0]][r[1]]

        rscore = pts[r[1]] + win[r[1]][r[0]]
        score += rscore

print(score)