#! /usr/bin/env python

map2 = {'A':'R', 'B':'P', 'C':'S'}
map = {'X':'A', 'Y':'B', 'Z':'C'}
pts = {'A':1, 'B':2, 'C':3}

win = {'A':{'A':3, 'B':0, 'C':6},
       'B':{'A':6, 'B':3, 'C':0},
       'C':{'A':0, 'B':6, 'C':3}}

score = 0
ct = 0
with open("input.txt") as f:
    for line in f:
        r = line.strip().split(" ")
        r[1] = map[r[1]]
        rscore = pts[r[1]] + win[r[1]][r[0]]
        score += rscore

print(score)