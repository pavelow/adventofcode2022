#! /usr/bin/env python

count = 0

with open("input.txt") as f:
    for line in f:
        e = []
        for elf in line.strip().split(","):
            elf = elf.split("-")
            e.append(set(range(int(elf[0]), int(elf[1])+1)))
        if (e[0].issubset(e[1]) or e[1].issubset(e[0])):
            count += 1

print(count)