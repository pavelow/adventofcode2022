#! /usr/bin/env python

from collections import deque

stacks = []
nstacks = 0
instructions = []

with open("input.txt") as f:
    nstacks = (len(f.readline())//4)
    f.seek(0)

    for i in range(nstacks):
        stacks.append(deque())

    # parse stacks
    for l in f:
        if l[1] == "1":
            break

        for i in range(nstacks):
            a = list(l[i*4:(i*4)+4])[1]
            if(a != ' '):
                stacks[i].appendleft(a)

    f.readline()

    # run instructions
    for inst in f:
        inst = inst.strip().split(' ')

        for i in range(int(inst[1])):
            stacks[int(inst[5])-1].append(stacks[int(inst[3])-1].pop())

for i in range(nstacks):
    print(stacks[i][-1], end='')
print()