#! /usr/bin/env python

data = ""

with open("input.txt") as f:
    data = f.readline().strip()


s = set()
last = 0
for i in range(len(data)):
    s.add(data[i])

    if last == len(s):
        s = set()
        last = 0
    last = len(s)
    
    if (len(s) == 4):
        print(i)
        break