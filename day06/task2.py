#! /usr/bin/env python

data = ""

with open("input.txt") as f:
    data = f.readline().strip()


s = set()
last = 0

i = 0
while i < len(data):
    s.add(data[i])
    print(s)

    if last == len(s):
        t = data[i]
        i -= 1
        
        while data[i] != t:
            i -= 1
        s = set()
        last = 0
    else:
        last = len(s)
    
    i += 1
    
    if (len(s) == 14):
        print(i)
        break